# This command synchronize the dotfiles repo with my dotfiles on the home folder
# Run this script while on the main folder or change the paths accordingly to your execution dir
# -a sync preserving all file system attributes
# -v run verbosely
# -u only copy file with a newer modification time or size difference
# For more info read the rsync man page
rsync -avu --exclude ".git/" --exclude "sync.sh" . ~

