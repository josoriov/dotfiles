" Basic settings for nvim config
syntax enable

" Accesibility
set mouse=a
set clipboard=unnamed
set encoding=utf8
" Editor
set number
set numberwidth=1
set showcmd
set ruler
set cursorline
set showmatch
set smartindent
set ts=2 sw=2
set laststatus=2
set incsearch
" set relativenumber
" set colorcolumn=80
" File specifics
set noswapfile
set nobackup
set undodir=~/.vim/undodir/
set undofile


" Not showing mode because of the lightline implementation (see plugins)
set noshowmode

" Plugins installation
call plug#begin('~/.vim/plugged')

" " Themes
" Plug 'drewtempelmeyer/palenight.vim'
Plug 'morhetz/gruvbox'
Plug 'wadackel/vim-dogrun'
Plug 'itchyny/lightline.vim'
" " IDE
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
Plug 'christoomey/vim-tmux-navigator'
Plug 'vim-python/python-syntax'
Plug 'ap/vim-css-color'
Plug 'mbbill/undotree'
Plug 'jreybert/vimagit'
Plug 'ycm-core/YouCompleteMe'
Plug 'mhinz/vim-startify'

" Finishing plugins installation
call plug#end()

" Plugins loading
" " Themes
set t_Co=256
colorscheme gruvbox
let g:lightline = {'colorscheme': 'deus'}
" " Background


" " IDE
" " Nerdtree quits after opening file
let NERDTreeQuitOnOpen=1
" " Python syntax
let g:python_highlight_all=1

" Keybindings

" " Leader key defined as SPC
let mapleader=" "

" " Navigation

" " SPC-s-s easymotion search
nmap <Leader>ss <Plug>(easymotion-s2)
" " SPC-s-c clear search highlight
nmap <Leader>sc :noh<CR>
" " SPC-o-t open nerdtree
nmap <Leader>ot :NERDTreeToggle<CR>
" " SPC-o-u open Undotree
nmap <Leader>ou :UndotreeToggle<CR>


" " Window management 
" " Overwrite default tmux navigator shortcuts
let g:tmux_navigator_no_mappings = 1
" " Move to left window 
nnoremap <silent> <Leader>wh :TmuxNavigateLeft<CR>
" " Move to down window 
nnoremap <silent> <Leader>wj :TmuxNavigateDown<CR>
" " Move to up window 
nnoremap <silent> <Leader>wk :TmuxNavigateUp<CR>
" " Move to right window 
nnoremap <silent> <Leader>wl :TmuxNavigateRight<CR>
" " Move to previous window 
nnoremap <silent> <Leader>wp :TmuxNavigatePrevious<CR>
" " Close current window
nnoremap <Leader>wc :close<CR>
" " Vertical split
nnoremap <Leader>wv :vsplit<CR>
" " Horizontal split
nnoremap <Leader>ws :split<CR>

" " File Management
" " Save file
nnoremap <Leader>fw :w<CR>
" " Save and quit
nnoremap <Leader>qw :wq<CR>
" " Quit the editor (must save before)
nnoremap <Leader>qq :q<CR>
