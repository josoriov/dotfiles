;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "juanferov"
      user-mail-address "juanferov97@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(use-package doom-themes
  :config
  ;; Global settings
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  (load-theme 'doom-gruvbox t)

  ;; Flashing mode-line on errors
  ;; (doom-themes-visual-bell-config)

  ;; Enable custom treemacs theme (require all-the-icons)
  (setq doom-themes-treemacs-theme "doom-colors")
  (doom-themes-treemacs-config)

  ;; Improves org-mode's native fontification
  (doom-themes-org-config))

;; Indentation guides
(setq tab-width 4)
(setq tab-stop-list (number-sequence 4 200 4))


;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Programming/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `After!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Changes to doom emacs looks
(setq doom-font (font-spec :family "Roboto Mono Medium" :size 15)
      doom-variable-pitch-font (font-spec :family "Roboto Mono Medium" :size 15))

(setq projectile-project-search-path '("~/Programming/personal/" "~/Programming/sandbox/" "~/Programming/study/"))

;; Changes to doom emacs keybindings

;; Generalities
;;; Copy to clipboard
(map! :leader
      :desc "Copy to system clipboard"
      "i c c" #'clipboard-kill-ring-save)
;;; Paste from clipboard
(map! :leader
      :desc "Paste from system clipboard"
      "i c p" #'clipboard-yank)

;; Project Management
;; Toggle treemacs on SPC o t
(map! :leader
      :desc "Toggle treemacs"
      "o t" #'treemacs)
;; Remove current project from treemacs workspace
(map! :leader
      :desc "Remove project from workspace"
      "p m r" #'treemacs-remove-project-from-workspace)
;; Add project to treemacs workspace
(map! :leader
      :desc "Add project to workspace"
      "p m a" #'treemacs-projectile)

;; File Management
;;; Save file
(map! :leader
      :desc "Write to file (Save)"
      "f w" #'evil-write)

;;; Open a new eshell instance
(map! :leader
      :desc "Open shell"
      "o e" #'eshell)

;; LISP repl
;;; Sly eval and pretty print
(map! :leader
      :desc "Eval region and pretty print"
      "m e R" #'sly-pprint-eval-region)

; Adding hooks to Emacs
;; Enabling file icons in dired
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

